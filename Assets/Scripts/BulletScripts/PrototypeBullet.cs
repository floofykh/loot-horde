using UnityEngine;
using System.Collections;

public class PrototypeBullet : MonoBehaviour
{
	private static float speed, distance;
	private static bool staticMembersInitialised = false;
	private Vector3 startingPos;
	public GameObject prefab;

	// Use this for initialization
	void Awake ()
	{
		if(!staticMembersInitialised)
		{
			ResourceManager resourceManager = Camera.main.GetComponent<ResourceManager>();
			speed = float.Parse(resourceManager.GetResource("BulletData/PrototypeBullet", "speed"));
			distance = float.Parse(resourceManager.GetResource("BulletData/PrototypeBullet", "distance"));
			staticMembersInitialised = true;
		}
	}

	void Update()
	{
		if(Mathf.Abs((gameObject.transform.position - startingPos).magnitude) >= distance)
		{
			rigidbody2D.velocity = new Vector2(0.0f, 0.0f);
			Camera.main.GetComponent<GameObjectPool>().AddToPool(gameObject, prefab);
			Debug.Log("Bullet's max distance reached. Destroyed.");
		}
	}

	public void SetDirection(Vector2 direction)
	{
		rigidbody2D.velocity = direction*speed;
		startingPos = gameObject.transform.position;
	}
}

