using UnityEngine;
using System.Collections;

public class DragStaticTouchJoystick : IStaticJoystick
{
	private enum State
	{
		INACTIVE,
		TOUCHED,
		DRAGGING
	};
	private Texture2D texture;
	private Vector2 position;
	private float radius;
	private State state;
	private Vector2 touchDownPoint;
	private int touchID;
	private float min, max;

	Texture2D IStaticJoystick.Texture{set{texture = value;}}
	Vector2 IStaticJoystick.Position{set{position = value;}}
	float IStaticJoystick.Radius{set{radius = value;}}

	public DragStaticTouchJoystick()
	{
		ResourceManager resourceManager = Camera.main.GetComponent<ResourceManager>();
		if(resourceManager == null)
			Debug.LogError("ResourceManager not found");

		min = float.Parse(resourceManager.GetResource("Controls/DragStaticTouchJoystick", "min"));
		max = float.Parse(resourceManager.GetResource("Controls/DragStaticTouchJoystick", "max"));
		if(max == 0 || min > max || min == max)
			Debug.LogError("Min and Mix values not initialised correctly");
	}
	
	Vector2 IStaticJoystick.GetInputResult()
	{
		//Get the current touches on the screen.
		Touch [] touches = Input.touches;//Iterate through all touches on the screen.
		
		switch(state)
		{
		case State.INACTIVE:
			Inactive (touches);
			break;
		case State.TOUCHED:
			Touched (touches);
			break;
		case State.DRAGGING:
			Vector2 value = Dragging (touches);
			value = new Vector2(value.x, -value.y);
			return value;
		};

		return new Vector2(0.0f, 0.0f);
	}
	
	void IStaticJoystick.DrawToGUI()
	{
		//Draw move joystick
		if(texture != null)
			GUI.DrawTexture(new Rect(position.x - radius, position.y - radius, 
			                         radius*2, radius*2), texture);
	}

	private void Inactive(Touch[] touches)
	{
		for(int i=0; i<touches.Length; i++)
		{
			Vector2 touch = TouchToScreenCoords(touches[i]);

			if(GeometryAlgebra.PointLiesInCircle(touch, position, radius))
			{
				touchDownPoint = touch;
				touchID = touches[i].fingerId;
				state = State.TOUCHED;
				Debug.Log("Joystick state changed to TOUCHED");
			}
		}
	}

	private void Touched(Touch[] touches)
	{
		bool sameTouchPresent = false;

		for(int i=0; i<touches.Length; i++)
		{
			if(touches[i].fingerId == touchID)
			{
				sameTouchPresent = true;

				if((TouchToScreenCoords(touches[i]) - touchDownPoint).magnitude >= radius*min)
				{
					state = State.DRAGGING;
					Debug.Log("Joystick state changed to DRAGGING");
				}
				break;
			}
		}
		if(!sameTouchPresent)
		{
			state = State.INACTIVE;
			Debug.Log("Joystick state changed to INACTIVE");
		}
	}

	private Vector2 Dragging(Touch[] touches)
	{
		for(int i=0; i<touches.Length; i++)
		{
			if(touches[i].fingerId == touchID)
			{
				Vector2 difference = TouchToScreenCoords(touches[i]) - touchDownPoint;
				if(difference.magnitude >= radius*max)
				{
					difference.Normalize();
					difference = difference*radius*max;
				}
				return difference/(radius*max);
			}
		}

		state = State.INACTIVE;
		Debug.Log("Joystick state changed to INACTIVE");
		return new Vector2(0.0f, 0.0f);
	}

	private Vector2 TouchToScreenCoords(Touch touch)
	{
		return new Vector2(touch.position.x, Screen.height - touch.position.y);
	}
}

