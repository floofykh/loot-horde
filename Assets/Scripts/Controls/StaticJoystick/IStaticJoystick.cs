using UnityEngine;
using System.Collections;

public interface IStaticJoystick
{
	Texture2D Texture{set;}
	Vector2 Position{set;}
	float Radius{set;}

	Vector2 GetInputResult();
	void DrawToGUI();
}

