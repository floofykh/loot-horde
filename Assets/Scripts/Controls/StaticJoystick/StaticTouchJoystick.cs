using UnityEngine;
using System.Collections;

public class StaticTouchJoystick : IStaticJoystick
{
	private Texture2D texture;
	private Vector2 position;
	private float radius;

	Texture2D IStaticJoystick.Texture{set{texture = value;}}
	Vector2 IStaticJoystick.Position{set{position = value;}}
	float IStaticJoystick.Radius{set{radius = value;}}

	Vector2 IStaticJoystick.GetInputResult()
	{
		//Get the current touches on the screen.
		Touch [] touches = Input.touches;//Iterate through all touches on the screen.

		for(int i=0; i<touches.Length; i++)
		{
			//Convert the touch coordinates to screen space.
			Vector2 touch = new Vector2(touches[i].position.x, Screen.height - touches[i].position.y);
			
			//Check if touch lies inside the joystick.
			if(GeometryAlgebra.PointLiesInCircle(touch, position, radius))
			{
				//Get the direction of the touch in relation to the circle centre, and normalise it.
				Vector2 direction = (touch - position) / radius;
				//Invert the y coordinate, because touch screens.
				direction = new Vector2(direction.x, -direction.y);
				//Move the player in that direction.
				return direction;
			}
		}
		return new Vector2(0.0f, 0.0f);
	}

	void IStaticJoystick.DrawToGUI()
	{
		//Draw move joystick
		if(texture != null)
			GUI.DrawTexture(new Rect(position.x - radius, position.y - radius, 
			                         radius*2, radius*2), texture);
	}
}

