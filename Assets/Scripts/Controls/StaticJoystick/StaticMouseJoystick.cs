using UnityEngine;
using System.Collections;

public class StaticMouseJoystick : IStaticJoystick
{
	private Texture2D texture;
	private Vector2 position;
	private float radius;
	
	Texture2D IStaticJoystick.Texture{set{texture = value;}}
	Vector2 IStaticJoystick.Position{set{position = value;}}
	float IStaticJoystick.Radius{set{radius = value;}}
	
	Vector2 IStaticJoystick.GetInputResult()
	{
		if(Input.GetMouseButton(0))
		{
			Vector2 mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
			
			if(GeometryAlgebra.PointLiesInCircle(mouse, position, radius))
			{
				Vector2 direction = (mouse - position) / radius;
				
				direction = new Vector2(direction.x, -direction.y);
				return direction;
			}
		}

		return new Vector2(0.0f, 0.0f);
	}
	
	void IStaticJoystick.DrawToGUI()
	{
		//Draw move joystick
		if(texture != null)
			GUI.DrawTexture(new Rect(position.x - radius, position.y - radius, 
			                         radius*2, radius*2), texture);
	}
}

