using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
	private float acceleration;
	private Vector2 forceApplied;
	private float maxSpeed;

	// Use this for initialization
	void Start ()
	{
		ResourceManager resourceManager = GameObject.Find("Camera").GetComponent<ResourceManager>();
		if(resourceManager != null)
		{
			acceleration = float.Parse(resourceManager.GetResource("PlayerData", "acceleration"));
			maxSpeed = float.Parse(resourceManager.GetResource("PlayerData", "maxSpeed"));
		}
	}

	void FixedUpdate ()
	{
		rigidbody2D.AddForce(forceApplied);
		forceApplied = new Vector2(0.0f, 0.0f);
		if(rigidbody2D.velocity.magnitude > maxSpeed)
		{
			rigidbody2D.velocity = rigidbody2D.velocity.normalized * maxSpeed;
		}

		if(rigidbody2D.velocity.magnitude < 0.1f)
		{
			rigidbody2D.velocity = new Vector2(0.0f, 0.0f);
		}
	}

	public void Move(Vector3 direction)
	{
		forceApplied += (Vector2)direction * acceleration;
	}
}

