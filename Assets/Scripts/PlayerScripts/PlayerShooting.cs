using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour
{
	private IWeapon weapon;
	public IWeapon Weapon{set{weapon = value;}}
	public GameObject BulletPrefab{get; set;}

	//Temporary for prototype
	public GameObject bullet;

	// Use this for initialization
	void Start ()
	{
		BulletPrefab = bullet;
		//Temporary for Prototype
		weapon = new PrototypeWeapon();
		weapon.Init();
	}

	// Update is called once per frame
	void Update ()
	{
		weapon.Update();
	}

	public void Shoot(Vector2 direction)
	{
		weapon.Shoot(direction, gameObject);
	}
}

