using UnityEngine;
using System.Collections;

public class PlayerInput_MouseJoystick : MonoBehaviour
{
	private PlayerMovement playerMovement;
	public Texture2D moveStick, shootStick;
	private IStaticJoystick moveJoystick, shootJoystick;
	
	// Use this for initialization
	void Start ()
	{
		moveJoystick = new StaticMouseJoystick();
		shootJoystick = new StaticMouseJoystick();
		
		playerMovement = gameObject.GetComponent<PlayerMovement>();
		if(playerMovement == null)
			Debug.LogError("PlayerMovement script not found on player.");
		
		float joystickRadius = Screen.width/10.0f;
		
		moveJoystick.Position = new Vector2(joystickRadius, Screen.height - joystickRadius);
		moveJoystick.Radius = joystickRadius;
		moveJoystick.Texture = moveStick;
		
		shootJoystick.Position = new Vector2(Screen.width - joystickRadius, Screen.height - joystickRadius);
		shootJoystick.Radius = joystickRadius;
		shootJoystick.Texture = shootStick;
	}
	
	void Update()
	{
		Vector2 moveDirection = moveJoystick.GetInputResult();
		Vector2 shootDirection = shootJoystick.GetInputResult();
		
		playerMovement.Move(moveDirection);
		
		if(shootDirection.magnitude != 0)
			GetComponent<PlayerShooting>().Shoot(shootDirection);
	}
	
	void OnGUI()
	{
		moveJoystick.DrawToGUI();
		shootJoystick.DrawToGUI();
	}
}

