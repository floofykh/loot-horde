#if UNITY_EDITOR

using UnityEngine;
using System.Collections;

public class PlayerInput_Mouse : MonoBehaviour
{
	private PlayerShooting playerShooting;

	// Use this for initialization
	void Start ()
	{
		playerShooting = gameObject.GetComponent<PlayerShooting>();
		
		if(playerShooting == null)
			Debug.LogError("PlayerShooting script not found on player.");
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetMouseButton(0))
		{
			Vector2 mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
			Vector2 relativePosition = mouse - (Vector2) Camera.main.WorldToScreenPoint(gameObject.transform.position);
			relativePosition.Normalize();
			Vector2 direction = new Vector2(relativePosition.x, -relativePosition.y);
			playerShooting.Shoot(direction);
		}
	}
}

#endif