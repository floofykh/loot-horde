#if UNITY_EDITOR

using UnityEngine;
using System.Collections;

public class PlayerInput_Keyboard : MonoBehaviour
{
	private PlayerMovement playerMovement;

	// Use this for initialization
	void Start ()
	{
		playerMovement = gameObject.GetComponent<PlayerMovement>();

		if(playerMovement == null)
			Debug.LogError("PlayerMovement script not found on player.");
	}

	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKey("w"))
		{
			playerMovement.Move(new Vector2(0.0f, 1.0f));
		}
		if(Input.GetKey("s"))
		{
			playerMovement.Move(new Vector2(0.0f, -1.0f));
		}
		if(Input.GetKey("a"))
		{
			playerMovement.Move(new Vector2(-1.0f, 0.0f));
		}
		if(Input.GetKey("d"))
		{
			playerMovement.Move(new Vector2(1.0f, 0.0f));
		}
	}
}

#endif

