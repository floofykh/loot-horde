using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
	private GameObject mainCamera;
	private int cameraZ;

	// Use this for initialization
	void Start ()
	{
		mainCamera = GameObject.Find("Camera");

		if(mainCamera == null)
		{
			Debug.LogError("Camera could not be found.");
		}

		ResourceManager resourceManager = mainCamera.GetComponent<ResourceManager>();
		cameraZ = int.Parse(resourceManager.GetResource("CameraData", "zPos"));
	}

	// Update is called once per frame
	void Update ()
	{
		Vector3 playerPos = transform.position;
		mainCamera.transform.position = new Vector3(playerPos.x, playerPos.y, cameraZ);
	}
}

