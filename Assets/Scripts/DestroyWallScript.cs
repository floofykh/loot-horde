using UnityEngine;
using System.Collections;

public class DestroyWallScript : MonoBehaviour
{
	public GameObject floorPrefab, wallPrefab;
	private static GameObjectPool pool;
	private static bool initialisedStaticVariables = false;

	void Start()
	{
		if(!initialisedStaticVariables)
		{
			pool = GameObject.Find("Camera").GetComponent<GameObjectPool>();
			
			if(pool == null)
				Debug.LogError("GameObjectPool could not be found");
			else
				initialisedStaticVariables = true;
		}
	}

	public void DestroyWall()
	{
		GameObject floor = pool.GetInstanceOfPrefab(floorPrefab);
		floor.transform.position = transform.position;

		pool.AddToPool(gameObject, wallPrefab);
	}
}

