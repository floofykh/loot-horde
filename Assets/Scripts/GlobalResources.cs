using UnityEngine;
using System.Xml.Linq;

public static class GlobalResources
{
	/*private static int maxRoomSize_Large, minRoomSize_Large;
	public static int MaxRoomSize_Large{get{return maxRoomSize_Large;}}
	public static int MinRoomSize_Large{get{return minRoomSize_Large;}}

	private static int maxRoomSize_Side, minRoomSize_Side;
	public static int MaxRoomSize_Side{get{return maxRoomSize_Side;}}
	public static int MinRoomSize_Side{get{return minRoomSize_Side;}}

	private static int maxRoomSize_Path, minRoomSize_Path;
	public static int MaxRoomSize_Path{get{return maxRoomSize_Path;}}
	public static int MinRoomSize_Path{get{return minRoomSize_Path;}}

	private static GameObject playerPrefab;
	public static GameObject PlayerPrefab {get{return playerPrefab;}}
	public static GameObject PlayerObject{get; set;}

	private static float cameraZ;
	public static float CameraZ{get{return cameraZ;}}

	public static bool Init()
	{
		XDocument globalResData;
		try
		{
			globalResData = XDocument.Load(Application.dataPath + "/XML Data/ApplicationResources.xml");
		}	
		catch(System.Exception e)
		{
			Debug.LogException(e);
			return false;
		}

		XElement applicationDataElement = globalResData.Element("ApplicationResources");
		XElement roomDataElement = applicationDataElement.Element("RoomData");

		XElement largeRoomElement, sideRoomElement, pathRoomElement, cameraElement;
		try
		{
			largeRoomElement = roomDataElement.Element("LargeRoom");
			sideRoomElement = roomDataElement.Element("SideRoom");
			pathRoomElement = roomDataElement.Element("PathRoom");
			cameraElement = applicationDataElement.Element("CameraData");
		}
		catch(System.Exception e)
		{
			Debug.LogException(e);
			return false;
		}
		maxRoomSize_Large = int.Parse(largeRoomElement.Attribute("maxSize").Value);
		minRoomSize_Large = int.Parse(largeRoomElement.Attribute("minSize").Value);

		maxRoomSize_Side = int.Parse(sideRoomElement.Attribute("maxSize").Value);
		minRoomSize_Side = int.Parse(sideRoomElement.Attribute("minSize").Value);

		maxRoomSize_Path = int.Parse(pathRoomElement.Attribute("maxSize").Value);
		minRoomSize_Path = int.Parse(pathRoomElement.Attribute("minSize").Value);

		cameraZ = float.Parse(cameraElement.Attribute("zPos").Value);

		playerPrefab = Resources.Load("Prefabs/Actors/Player") as GameObject;
		if(playerPrefab == null)
			return false;

		return true;
	}*/
}

