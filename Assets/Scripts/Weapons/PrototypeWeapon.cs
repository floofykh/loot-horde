using UnityEngine;
using System.Collections;

public class PrototypeWeapon : IWeapon
{
	private const float delay = 0.5f;
	private float lastTime;
	private GameObject bulletPrefab;
	private GameObjectPool pool;

	void IWeapon.Init()
	{
		lastTime = 0;
		pool = GameObject.Find("Camera").GetComponent<GameObjectPool>();
	}

	void IWeapon.Update()
	{
		
	}

	void IWeapon.DrawGUI()
	{
		
	}

	void IWeapon.Shoot(Vector2 direction, GameObject player)
	{
		if(Time.time - lastTime >= delay)
		{
			if(bulletPrefab == null)
				bulletPrefab = player.GetComponent<PlayerShooting>().BulletPrefab;
			
			GameObject bullet = pool.GetInstanceOfPrefab(bulletPrefab);
			bullet.transform.position = player.transform.position;
			bullet.SendMessage("SetDirection", direction);

			lastTime = Time.time;
		}
	}

	void IWeapon.Destroy()
	{
		
	}

}

