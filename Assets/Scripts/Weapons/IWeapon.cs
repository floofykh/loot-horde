using UnityEngine;
using System.Collections;

public interface IWeapon
{
	void Init();
	void Update();
	void DrawGUI();
	void Shoot(Vector2 direction, GameObject player);
	void Destroy();
}

