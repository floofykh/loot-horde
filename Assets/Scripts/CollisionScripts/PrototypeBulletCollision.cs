using UnityEngine;
using System.Collections;

public class PrototypeBulletCollision : MonoBehaviour
{
	private GameObjectPool pool;
	private GameObject bulletPrefab;

	void Start()
	{
		pool = Camera.main.GetComponent<GameObjectPool>();
		bulletPrefab = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerShooting>().BulletPrefab;
	}

	void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.gameObject.tag == "Wall")
		{
			gameObject.rigidbody2D.velocity = new Vector2(0.0f, 0.0f);
			pool.AddToPool(gameObject, bulletPrefab);

			collision.gameObject.SendMessage("DestroyWall");
		}
	}
}

