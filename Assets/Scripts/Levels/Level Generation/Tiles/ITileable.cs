using UnityEngine;
using System.Collections;

public interface ITileable
{
	GameObject GameObject{get;}
	string Tag{get;}
	Bounds CollisionBounds {get; set;}
	Vector3 Position{get; set;}
	Vector2 Rotation{get; set;}
	void Instantiate (Vector3 position);
	void Destroy();
	bool Colliding (ITileable other);
	void OnCollision(ITileable other);
}

