using UnityEngine;
using System.Collections;

public class FloorTile : ITileable
{
	private static GameObject floor = null;
	private GameObject gameObject = null;
	GameObject ITileable.GameObject{get{return gameObject;}}
	string ITileable.Tag{get{return "floor";}}

	Bounds ITileable.CollisionBounds{get{return new Bounds(new Vector3(), new Vector3());} set{}}
	Vector3 position;
	Vector2 rotation;
	Vector3 ITileable.Position{get{return position;} set{position = value; gameObject.transform.position = value;}}
	Vector2 ITileable.Rotation{get{return rotation;} set{rotation = value; gameObject.transform.rotation = Quaternion.Euler(value);}}

	public FloorTile ()
	{
		if(ReferenceEquals(null, floor))
		{
			floor = Resources.Load("Prefabs/Tiles/Floor") as GameObject;
		}
	}

	void ITileable.Instantiate (Vector3 position)
	{
		this.position = position;

		gameObject = GameObject.Instantiate(floor, position, Quaternion.Euler(rotation)) as GameObject;
	}

	bool ITileable.Colliding (ITileable other)
	{
		return false;
	}

	void ITileable.OnCollision(ITileable other)
	{
	}

	void ITileable.Destroy()
	{
		if(!ReferenceEquals(null, gameObject))
			GameObject.Destroy(gameObject);
	}
}

