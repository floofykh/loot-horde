using UnityEngine;
using System.Collections;

public class WallTile : ITileable
{
	private static GameObject wall = null;
	private GameObject gameObject = null;
	GameObject ITileable.GameObject{get{return gameObject;}}
	string ITileable.Tag{get{return "wall";}}

	Bounds collisionBounds = new Bounds(new Vector3(), new Vector3());
	Bounds ITileable.CollisionBounds{get{return collisionBounds;} set{collisionBounds = value;}}

	Vector3 position;
	Vector2 rotation;
	Vector3 ITileable.Position{get{return position;} set{position = value; gameObject.transform.position = value;}}
	Vector2 ITileable.Rotation{get{return rotation;} set{rotation = value; gameObject.transform.rotation = Quaternion.Euler(value);}}
	
	public WallTile ()
	{
		if(ReferenceEquals(null, wall))
		{
			wall = Resources.Load("Prefabs/Tiles/Wall") as GameObject;
		}
	}
	
	void ITileable.Instantiate (Vector3 position)
	{
		this.position = position;

		Vector3 offsetToCentre = new Vector3(1/2, 1/2);
		Vector3 centre = position + offsetToCentre;
		collisionBounds = new Bounds(centre, new Vector2(1.0f, 1.0f));
		
		gameObject = GameObject.Instantiate(wall, position, Quaternion.Euler(rotation)) as GameObject;
	}
	
	bool ITileable.Colliding (ITileable other)
	{
		if(collisionBounds != new Bounds(new Vector3(), new Vector3()) && other.CollisionBounds != new Bounds(new Vector3(), new Vector3()))
		{
			return collisionBounds.Intersects(other.CollisionBounds);
		}
		return false;
	}
	
	void ITileable.OnCollision(ITileable other)
	{
	}

	void ITileable.Destroy()
	{
		if(!ReferenceEquals(null, gameObject))
			GameObject.Destroy(gameObject);
	}
}

