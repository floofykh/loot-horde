using UnityEngine;
using System.Collections;

public interface ILevelGenerator
{
	bool Init (IMap map);
	bool GenerateLevel();
}

