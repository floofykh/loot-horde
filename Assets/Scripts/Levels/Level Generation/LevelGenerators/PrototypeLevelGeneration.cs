using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml.Linq;

public class PrototypeLevelGeneration : ILevelGenerator
{
	private IMap map;
	//Variables to hold the random distribution stats for  each room type.
	private int total, large, side;
	
	bool ILevelGenerator.Init(IMap map)
	{
		this.map = map;
		map.Init();
		LoadDistributionData();
		return true;
	}

	bool ILevelGenerator.GenerateLevel()
	{
		AddStartArea();

		for(int i=0; i<6; i++)
		{
			AddArea(new PathArea());
		}
			
		map.GenerateMap();

		return true;
	}

	private IMappable AddStartArea()
	{
		IMappable area;

		do
		{
			area = new LargeArea(new Vector2(Random.Range(0, 100), 
			                                 Random.Range(0, 100)));
			area.Init();
		}while(map.Overlaps(area));

		map.AddStartArea(area);
		return area;
	}

	private IMappable AddArea(IConnection connection)
	{
		//IMappable to create, add and return. 
		IMappable area = null;

		int randomNum = Random.Range(1, total+1);

		do
		{
			if(randomNum <= large)
			{
				//Generate a large area
				area = new LargeArea(new Vector2(Random.Range(0, 100), Random.Range(0, 100)));
				area.Init();
			}
			else if(randomNum <= large + side)
			{
				//Generate a side area
				area = new SideArea(new Vector2(Random.Range(0, 100), Random.Range(0, 100)));
				area.Init();
			}
			else
			{
				//Not allowed here, sneaky machine.
				Debug.LogWarning("Randomly generated number not within constraints of distribution.");
			}
		}while(area != null && map.Overlaps(area));

		map.AddArea(area, connection);
		return area;
	}

	private void AddConnection()
	{
		IConnection connection = new PathArea();

		map.AddConnection(connection);
	}

	private void LoadDistributionData()
	{
		ResourceManager resourceManager = GameObject.Find("Camera").GetComponent<ResourceManager>();
		if(resourceManager == null)
		{
			Debug.LogError("Couldn't retrieve Resource Manage");
			return;
		}

		var resources = resourceManager.GetResources("LevelData/DefaultLevel/RoomDistributions");

		if(resources != null)
		{
			large = int.Parse(resources["large"]);
			side = int.Parse(resources["side"]);
			total = large + side;
		}
	}
}
