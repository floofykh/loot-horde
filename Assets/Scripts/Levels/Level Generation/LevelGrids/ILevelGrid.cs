using UnityEngine;
using System.Collections;

public interface ILevelGrid
{
	bool Init(Rect bounds);
	bool SetTile(Vector3 index, ITileable tile);
}

