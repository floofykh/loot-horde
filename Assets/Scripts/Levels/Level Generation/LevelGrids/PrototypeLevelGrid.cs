using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PrototypeLevelGrid : ILevelGrid
{
	private List<ITileable [,]> grid;

	private Rect bounds;

	public PrototypeLevelGrid()
	{
		grid = new List<ITileable[,]>();
	}

	bool ILevelGrid.Init(Rect bounds)
	{
		this.bounds = bounds;
		AddLevel();
		FillLevelWithWalls(0);
		return true;
	}

	bool ILevelGrid.SetTile(Vector3 index, ITileable tile)
	{
		if(index.z < 0)
			return false;

		//This won't end well :/
		while(index.z > grid.Count)
			AddLevel();

		ITileable indexedTile = grid[(int)index.z][(int)index.x - (int)bounds.xMin, (int)index.y - (int)bounds.yMin];

		if(indexedTile != null)
			indexedTile.Destroy();

		grid[(int)index.z][(int)index.x - (int)bounds.xMin, (int)index.y - (int)bounds.yMin] = tile;
		tile.Instantiate(index);

		return true;
	}

	private void AddLevel()
	{
		grid.Add(new ITileable[(int)bounds.width, (int)bounds.height]);
		ClearGrid(grid.Count-1);
	}

	private void ClearGrid(int level)
	{
		if(grid != null && grid.Count > level)
		{
			for(int x=0; x<bounds.width; x++)
			{
				for(int y=0; y<bounds.height; y++)
				{
					grid[level][x,y] = null;
				}
			}
		}
	}

	private void FillLevelWithWalls(int level)
	{
		for(int x=0; x<bounds.width; x++)
		{
			for(int y=0; y<bounds.height; y++)
			{
				if(grid[level][x,y] != null)
					grid[level][x,y].Destroy();

				ITileable newWall = new WallTile();
				newWall.Instantiate(new Vector3(x + bounds.xMin, y + bounds.yMin, level));
				grid[level][x,y] = newWall;
			}
		}
	}
}

