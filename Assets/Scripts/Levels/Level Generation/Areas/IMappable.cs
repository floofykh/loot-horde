using UnityEngine;
using System.Collections;

public interface IMappable
{
	Vector2 Size {get; set;}
	Vector2 Position {get; set;}
	MappableType Type {get;}

	bool Init();
	bool Overlaps (IMappable other);
	void Generate (IMap map);
	Vector2 MiddleBottom();
	Vector2 LeftMiddle();
	Vector2 MiddleTop();
	Vector2 RightMiddle();
}

public enum MappableType
{
	MAIN_AREA,
	SIDE_AREA,
	HIDDEN_AREA,
	CONNECTION
}

