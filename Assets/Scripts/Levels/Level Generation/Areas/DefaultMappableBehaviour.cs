using UnityEngine;
using System.Collections;

public static class DefaultMappableBehaviour
{
	public static bool Init(string name, ref bool staticVariablesInitialised, ref int minRoomSize, ref int maxRoomSize, ref Vector2 size)
	{
		if(!staticVariablesInitialised)
		{
			ResourceManager resourceManager = GameObject.Find("Camera").GetComponent<ResourceManager>();
			if(resourceManager == null)
				return false;
			
			var attributes = resourceManager.GetResources("RoomData/" + name);
			if(attributes == null)
				return false;
			
			minRoomSize = int.Parse(attributes["minSize"]);
			maxRoomSize = int.Parse(attributes["maxSize"]);
		}

		size = new Vector2(Random.Range(minRoomSize, maxRoomSize), 
		                   Random.Range(minRoomSize, maxRoomSize));
		return true;
	}

	public static bool Overlaps(IMappable thisArea, IMappable otherArea)
	{
		Rect thisRect = new Rect(thisArea.Position.x, thisArea.Position.y, thisArea.Size.x, thisArea.Size.y);
		Rect otherRect = new Rect(otherArea.Position.x, otherArea.Position.y, otherArea.Size.x, otherArea.Size.y);
		
		return thisRect.Overlaps(otherRect);
	}

	public static Vector2 MiddleBottom(IMappable area)
	{
		return new Vector2(area.Position.x + area.Size.x/2, area.Position.y + area.Size.y);
	}
	public static Vector2 LeftMiddle(IMappable area)
	{
		return new Vector2(area.Position.x, area.Position.y + area.Size.y/2);
	}
	public static Vector2 MiddleTop(IMappable area)
	{
		return new Vector2(area.Position.x + area.Size.x/2, area.Position.y);
	}
	public static Vector2 RightMiddle(IMappable area)
	{
		return new Vector2(area.Position.x + area.Size.x, area.Position.y + area.Size.y/2);
	}
}

