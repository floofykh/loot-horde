using UnityEngine;

public class LargeArea : IMappable
{
	Vector2 size, position;
	Vector2 IMappable.Size {get{return size;} set{size = value;}}
	Vector2 IMappable.Position {get{return position;} set{position = value;}}

	MappableType IMappable.Type {get{return MappableType.MAIN_AREA;}}

	private static int minRoomSize, maxRoomSize;
	private static bool staticVariablesInitialised = false;

	public LargeArea()
	{
		position = new Vector2();
		size = new Vector2();
	}

	public LargeArea(Vector2 position)
	{
		this.position = position;
		size = new Vector2();
	}

	bool IMappable.Init()
	{
		return DefaultMappableBehaviour.Init("LargeRoom", ref staticVariablesInitialised, ref minRoomSize, ref maxRoomSize, ref size);
	}

	bool IMappable.Overlaps (IMappable other)
	{
		return DefaultMappableBehaviour.Overlaps(this, other);
	}

	void IMappable.Generate (IMap map)
	{
		ILevelGrid grid = map.Grid;

		for(int x=(int)position.x; x<(int)position.x + (int)size.x; x++)
		{
			for(int y=(int)position.y; y<(int)position.y + (int)size.y; y++)
			{
				ITileable newTile;

				if(x == (int)position.x || x == (int)position.x + (int)size.x-1 || y == (int)position.y || y == (int)position.y + (int)size.y-1)
				{
					newTile = new WallTile();
				}
				else
				{
					newTile = new FloorTile();
				}

				grid.SetTile(new Vector2(x, y), newTile);
			}
		}
	}

	
	Vector2 IMappable.MiddleBottom()
	{
		return DefaultMappableBehaviour.MiddleBottom(this);
	}
	Vector2 IMappable.LeftMiddle()
	{
		return DefaultMappableBehaviour.LeftMiddle(this);
	}
	Vector2 IMappable.MiddleTop()
	{
		return DefaultMappableBehaviour.MiddleTop(this);
	}
	Vector2 IMappable.RightMiddle()
	{
		return DefaultMappableBehaviour.RightMiddle(this);
	}
}
