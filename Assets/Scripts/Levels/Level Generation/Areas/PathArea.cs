using UnityEngine;
using System.Collections;

public class PathArea : IConnection
{
	private int lowerX, higherX, lowerY, higherY;
	private Vector2 size, position;
	Vector2 IMappable.Size {get{return size;} set{size = value;}}
	Vector2 IMappable.Position {get{return position;} set{position = value;}}

	Vector2 connectionPoint1, connectionPoint2;
	
	MappableType IMappable.Type {get{return MappableType.CONNECTION;}}
	
	private static int minRoomSize, maxRoomSize;
	private static bool staticVariablesInitialised = false;
	
	public PathArea()
	{
		size = new Vector2();
		position = new Vector2();
		connectionPoint1 = new Vector2(-1, -1);
		connectionPoint2 = new Vector2(-1, -1);
	}

	bool IMappable.Init()
	{
		return DefaultMappableBehaviour.Init("PathRoom", ref staticVariablesInitialised, ref minRoomSize, ref maxRoomSize, ref size);
	}
	
	bool IMappable.Overlaps (IMappable other)
	{
		return DefaultMappableBehaviour.Overlaps(this, other);
	}

	void IMappable.Generate(IMap map)
	{
		if(connectionPoint1 != new Vector2(-1,-1) && connectionPoint2 != new Vector2(-1,-1) && connectionPoint1 != connectionPoint2)
		{
			if(connectionPoint1.x == connectionPoint2.x)
			{
				GenerateVerticalPath(connectionPoint1, connectionPoint2, map);
			}
			else if(connectionPoint1.y == connectionPoint2.y)
			{
				GenerateHorizontalPath(connectionPoint1, connectionPoint2, map);
			}
			/*else
			{
				switch(Random.Range(0, 2))
				{
				case 0:
					GenerateHorizontalPath(connectionPoint1, new Vector2(connectionPoint2.x + (int)size.x/2, connectionPoint1.y), map);
					GenerateVerticalPath(new Vector2(connectionPoint2.x, connectionPoint1.y), connectionPoint2, map);
					break;
				case 1:
					GenerateVerticalPath(connectionPoint1, new Vector2(connectionPoint1.x, connectionPoint2.y + (int)size.y/2), map);
					GenerateHorizontalPath(new Vector2(connectionPoint1.x, connectionPoint2.y), connectionPoint2, map);
					break;
				}
			}*/
		}
	}

	private void GenerateHorizontalPath(Vector2 connectionPoint1, Vector2 connectionPoint2, IMap map)
	{
		for(int x=lowerX-1; x<=higherX; x++)
		{
			for(int y=lowerY - (int)size.y/2; y<higherY + (int)size.y/2; y++)
			{
				ITileable newTile;

				if(y == lowerY - (int)size.y/2 || y == lowerY + (int)size.y/2)
				{
					newTile = new WallTile();
				}
				else
				{
					newTile = new FloorTile();
				}
				
				
				map.Grid.SetTile(new Vector2(x, y), newTile);
			}
		}
	}
	
	private void GenerateVerticalPath(Vector2 connectionPoint1, Vector2 connectionPoint2, IMap map)
	{

		for(int x=lowerX - (int)size.x/2; x<higherX + (int)size.x/2; x++)
		{
			for(int y=lowerY-1; y<=higherY; y++)
			{
				ITileable newTile;
				
				if(x == lowerX - (int)size.x/2 || x == lowerX + (int)size.x/2)
				{
					newTile = new WallTile();
				}
				else
				{
					newTile = new FloorTile();
				}
				
				map.Grid.SetTile(new Vector2(x, y), newTile);
			}
		}
	}

	private void CalculateConnectionBounds()
	{
		if(connectionPoint1.x < connectionPoint2.x)
		{
			lowerX = (int)connectionPoint1.x;
			higherX = (int)connectionPoint2.x;
		}
		else
		{
			lowerX = (int)connectionPoint2.x;
			higherX = (int)connectionPoint1.x;
		}
		if(connectionPoint1.y < connectionPoint2.y)
		{
			lowerY = (int)connectionPoint1.y;
			higherY = (int)connectionPoint2.y;
		}
		else
		{
			lowerY = (int)connectionPoint2.y;
			higherY = (int)connectionPoint1.y;
		}
	}

	void IConnection.AddConnection(Vector2 position)
	{
		if(connectionPoint1 == new Vector2(-1, -1))
			connectionPoint1 = position;
		else if (connectionPoint2 == new Vector2(-1, -1))
		{
			connectionPoint2 = position;
			CalculateConnectionBounds();
			this.position = new Vector2(lowerX, lowerY);
		}
		else
			Debug.LogError("Attempting to give a path more than two connections.");
	}
	
	Vector2 IMappable.MiddleBottom()
	{
		return DefaultMappableBehaviour.MiddleBottom(this);
	}
	Vector2 IMappable.LeftMiddle()
	{
		return DefaultMappableBehaviour.LeftMiddle(this);
	}
	Vector2 IMappable.MiddleTop()
	{
		return DefaultMappableBehaviour.MiddleTop(this);
	}
	Vector2 IMappable.RightMiddle()
	{
		return DefaultMappableBehaviour.RightMiddle(this);
	}
}

