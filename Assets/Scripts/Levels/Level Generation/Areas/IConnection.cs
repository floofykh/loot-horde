using UnityEngine;
using System.Collections;

public interface IConnection : IMappable
{
	void AddConnection(Vector2 position);
}

