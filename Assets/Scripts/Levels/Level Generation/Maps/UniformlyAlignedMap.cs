using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UniformlyAlignedMap : IMap
{
	//List of all the mappables in this MapWeb. 
	private List <IMappable> areaList;
	//The beginning of the web, can be several, each with as many connections as you like. 
	private Node beginningNode;
	//The 2D array holding the map grid.
	private ILevelGrid grid;
	ILevelGrid IMap.Grid {get{return grid;} set{grid = value;}}
	//Diameter of the map
	private int width, height;
	private int maxRoomDistance, minRoomDistance;
	private ResourceManager resourceManager;

	public UniformlyAlignedMap()
	{
		areaList = new List<IMappable>();
		grid = new PrototypeLevelGrid();
		beginningNode = null;
	}

	bool IMap.Init()
	{
		resourceManager = GameObject.Find("Camera").GetComponent<ResourceManager>();
		if(resourceManager == null)
			return false;

		var resources = resourceManager.GetResources("MapData/UniformlyAlignedMap/MapDetails");
		if(resources == null || resources.Count == 0)
			return false;

		maxRoomDistance = int.Parse(resources["maxDistanceBetweenRooms"]);
		minRoomDistance = int.Parse(resources["minDistanceBetweenRooms"]);

		if(maxRoomDistance == 0 || minRoomDistance == 0)
			return false;

		return true;
	}

	bool IMap.Overlaps(IMappable mappable)
	{
		return Overlaps(mappable);
	}

	void IMap.AddStartArea(IMappable area)
	{
		//Check that the area exists, and that it's a main area. The set its position to the origin. This is what we build the map around. 
		if(area != null && area.Type == MappableType.MAIN_AREA)
		{
			area.Position = new Vector2(0.0f, 0.0f);
			AddBaseNode(area);
		}
	}

	void IMap.AddArea(IMappable area, IConnection connection)
	{
		//Check that the area and connection exist, and that the area being added isn't a connection.
		if(area != null && area.Type != MappableType.CONNECTION && connection != null)
		{
			bool added = false;
			Node currentNode = beginningNode;
			while (!added)
			{
				int randomNum = Random.Range(0, currentNode.paths.Count+1);

				switch(randomNum)
				{
				case 0:
					//Add new node, connected to this node.
					added = Connect(area, currentNode, connection);
					break;
				case 1: case 2: case 3: case 4:
					//Traverse the map along the corresponding path.
					currentNode = currentNode.paths[randomNum-1].GetOtherNode(currentNode);
					break;
				}
			}
		}
	}

	void IMap.AddConnection(IConnection connection)
	{

	}

	private bool Overlaps(IMappable mappable)
	{
		//Check if any area in the list overlaps, and return true if it does. 
		foreach(IMappable area in areaList)
		{
			if(area.Overlaps(mappable) && mappable.Overlaps(area))
				return true;
		}
		return false;
	}
	
	private void AddBaseNode(IMappable mappable)
	{
		//Ensure this mappable does not already exist. 
		if(!areaList.Contains(mappable))
		{
			areaList.Add(mappable);

			//Create the node and set it to the start node. 
			Node newBaseNode = new Node();
			newBaseNode.mappable = mappable;
			beginningNode = newBaseNode;

			Vector3 centre = (Vector3)mappable.Position;
			centre += (Vector3)mappable.Size/2;
			centre.z = -10;

			resourceManager.AddResource ("PlayerObject", GameObject.Instantiate(resourceManager.GetResource("PlayerPrefab"), centre, Quaternion.identity) as GameObject);
		}
	}

	private bool Connect(IMappable newArea, Node connectedNode, IConnection connection)
	{
		connection.Init();
		newArea.Init();

		if(newArea != null && connectedNode != null && connection != null && connectedNode.paths.Count < 4 && !areaList.Contains(newArea))
		{
			//Position the new area, and get the direction the path with be
			Link.Direction direction = PositionNewArea(newArea, connectedNode);

			if(Overlaps(newArea))
				return false;

			areaList.Add(newArea);
			areaList.Add(connection);

			Node newNode = CreateNode(newArea);

			CreateLink(direction, connection, connectedNode, newNode);
			ConnectAreas(newNode.mappable, connectedNode.mappable, connection);

			return true;
		}
		return false;
	}

	private Node CreateNode(IMappable mappable)
	{
		//Create the new node and set its area,
		Node newNode = new Node();
		newNode.mappable = mappable;
		return newNode;
	}

	private Link CreateLink(Link.Direction direction, IConnection mappable, Node previous, Node next)
	{
		//Create the new link and set its direction, mappable, and two nodes.
		Link newLink = new Link();
		newLink.direction = direction;
		newLink.mappable = mappable;
		newLink.previous = previous;
		newLink.next = next;

		//Set the paths in the two nodes.
		previous.paths.Add(newLink);
		next.paths.Add(newLink);


		return newLink;
	}

	private Link.Direction PositionNewArea(IMappable newArea, Node connectedNode)
	{
		List<Link.Direction> possibleDirections = GetPossibleDirections(connectedNode);
		Link.Direction randomDirection = possibleDirections[Random.Range(0, possibleDirections.Count)];

		int randomDistance = Random.Range(minRoomDistance, maxRoomDistance+1);
		newArea.Position = connectedNode.mappable.Position;

		switch(randomDirection)
		{
		case Link.Direction.BOTTOM_TO_TOP:
			newArea.Position += new Vector2((int)connectedNode.mappable.Size.x/2 - (int)newArea.Size.x/2, connectedNode.mappable.Size.y + randomDistance);
			break;
		case Link.Direction.TOP_TO_BOTTOM:
			newArea.Position -= new Vector2(-(int)connectedNode.mappable.Size.x/2 + (int)newArea.Size.x/2, newArea.Size.y + randomDistance);
			break;
		case Link.Direction.LEFT_TO_RIGHT:
			newArea.Position -= new Vector2(newArea.Size.x + randomDistance, -(int)connectedNode.mappable.Size.y/2 + (int)newArea.Size.y/2);
			break;
		case Link.Direction.RIGHT_TO_LEFT:
			newArea.Position += new Vector2(connectedNode.mappable.Size.x + randomDistance, (int)connectedNode.mappable.Size.y/2 - (int)newArea.Size.y/2);
			break;
		}

		return randomDirection;
	}

	private List<Link.Direction> GetPossibleDirections(Node node)
	{
		List <Link.Direction> possibleDirections = new List<Link.Direction>();
		possibleDirections.Add(Link.Direction.BOTTOM_TO_TOP);
		possibleDirections.Add(Link.Direction.LEFT_TO_RIGHT);
		possibleDirections.Add(Link.Direction.RIGHT_TO_LEFT);
		possibleDirections.Add(Link.Direction.TOP_TO_BOTTOM);
		
		foreach(Link link in node.paths)
		{
			possibleDirections.Remove(link.direction);
		}

		return possibleDirections;
	}

	private void ConnectAreas(IMappable area1, IMappable area2, IConnection connection)
	{
		Vector2 point1 = new Vector2(), point2 = new Vector2();

		if(ConnectionPointCalculator.CalculatePoints(area1, area2, out point1, out point2))
		{
			connection.AddConnection(point1);
			connection.AddConnection(point2);
		}
	}

	bool IMap.GenerateMap()
	{
		Rect mapBounds = new Rect(0.0f, 0.0f, 0.0f, 0.0f);

		//Get the dimensions of the map created by the areas. 
		foreach(IMappable area in areaList)
		{
			if(mapBounds.xMax < area.Position.x + area.Size.x)
				mapBounds.xMax = area.Position.x + area.Size.x;
			if(mapBounds.yMax < area.Position.y + area.Size.y)
				mapBounds.yMax = area.Position.y + area.Size.y;
			if(mapBounds.xMin > area.Position.x)
				mapBounds.xMin = area.Position.x;
			if(mapBounds.yMin > area.Position.y)
				mapBounds.yMin = area.Position.y;
		}

		grid.Init(mapBounds);

		foreach(IMappable area in areaList)
		{
			area.Generate(this);
		}

		return true;
	}

	//A class for the nodes of the web. These will be the rooms, and have references to the paths, or the links, to other nodes, if any.
	private class Node
	{
		public IMappable mappable;
		public List <Link> paths;

		public Node()
		{
			mappable = null;
			paths = new List <Link>();
		}
	}
	//A class fot the paths, or links, to different nodes. Contain references to two nodes, the one it comes from, and the one it's going to.
	private class Link
	{
		public IConnection  mappable;
		public Node previous, next;
		public Direction direction;
		
		public Link()
		{
			mappable = null;
			previous = null;
			next = null;
			direction = Direction.BOTTOM_TO_TOP;
		}

		public Node GetOtherNode(Node currentNode)
		{
			if(ReferenceEquals(currentNode, previous))
				return next;
			else
				return previous;
		}

		public enum Direction
		{
			TOP_TO_BOTTOM,
			BOTTOM_TO_TOP,
			LEFT_TO_RIGHT,
			RIGHT_TO_LEFT
		}
	}
}


