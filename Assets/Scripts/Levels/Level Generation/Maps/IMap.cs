using UnityEngine;
using System.Collections;

public interface IMap
{
	ILevelGrid Grid {get; set;}
	bool Init();
	void AddStartArea(IMappable area);
	void AddArea(IMappable area, IConnection connection);
	void AddConnection(IConnection connection);
	bool Overlaps(IMappable mappable);
	bool GenerateMap();
}

