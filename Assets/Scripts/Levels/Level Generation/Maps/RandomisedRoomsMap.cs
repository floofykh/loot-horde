using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomisedRoomsMap : IMap
{
	//List of all the mappables in this MapWeb. 
	private List <IMappable> areaList;
	//List of all the nodes in this MapWeb.
	private List <Node> nodeList;
	//The beginning of the web, can be several, each with as many connections as you like. 
	private List <Node> baseNodes;
	//The 2D array holding the map grid.
	private ILevelGrid grid;
	ILevelGrid IMap.Grid {get{return grid;} set{grid = value;}}
	//Diameter of the map
	private int width, height;

	public RandomisedRoomsMap()
	{
		areaList = new List<IMappable>();
		baseNodes = new List<Node>();
		nodeList = new List<Node>();
		grid = new PrototypeLevelGrid();
	}

	bool IMap.Init()
	{
		return true;
	}

	bool IMap.Overlaps(IMappable mappable)
	{
		//Check if any area in the list overlaps, and return true if it does. 
		foreach(IMappable area in areaList)
		{
			if(area.Overlaps(mappable) && mappable.Overlaps(area))
				return true;
		}
		return false;
	}

	void IMap.AddStartArea(IMappable area)
	{
		//Check that the area exists, and that it's a main area. The set its position to the origin. This is what we build the map around. 
		if(area != null && area.Type == MappableType.MAIN_AREA)
		{
			AddBaseNode(area);
		}
	}

	void IMap.AddArea(IMappable area, IConnection connection)
	{
		//Check that the area and connection exist, and that the area being added isn't a connection.
		if(area != null && area.Type != MappableType.CONNECTION && connection != null)
		{
			AddNode(area, connection, GetClosestMappable(area));
		}
	}

	void IMap.AddConnection(IConnection connection)
	{
		//Adds a connection between two random areas, given that there are at least 2 areas
		if(nodeList.Count > 1 && connection != null && connection.Type == MappableType.CONNECTION)
		{
			int index1, index2;
			index1 = Random.Range(0, nodeList.Count+1);
			index2 = Random.Range(0, nodeList.Count+1);
			while(index1 == index2)
			{
				index1 = Random.Range(0, nodeList.Count+1);
				index2 = Random.Range(0, nodeList.Count+1);
			}
			
			AddLink(nodeList[index1].mappable, nodeList[index2].mappable, connection);
		}
	}
	
	public void AddBaseNode(IMappable mappable)
	{
		//Ensure this mappable does not already exist. 
		if(!areaList.Contains(mappable))
		{
			areaList.Add(mappable);

			//Create the node and set it to the start node. 
			Node newBaseNode = new Node();
			newBaseNode.mappable = mappable;
			baseNodes.Add(newBaseNode);

			nodeList.Add(newBaseNode);
		}
	}

	public void AddNode(IMappable newArea, IConnection connection, IMappable connectedArea)
	{
		bool duplicate = false, connectedAreaExists = false;
		Node connectedNode = null;

		foreach(Node node in nodeList)
		{
			//Check if new node already exists, and set flag if it does.
			if(newArea == node.mappable)
				duplicate = true;

			//Ensure the area the new node will connect to exists, and flag if it does. Also store the node for modification later.
			if(connectedArea == node.mappable)
			{
				connectedNode = node;
				connectedAreaExists = true;
			}
		}

		//If it isn't a duplicate, and a node exists to connect to, create and add the node. 
		if(!duplicate && connectedAreaExists)
		{
			//Create a new node and set its mappable. 
			Node newNode = new Node();
			newNode.mappable = newArea;
			nodeList.Add(newNode);

			//Create a new link, set the link to the two nodes and set it's mappable. 
			Link newLink = new Link();
			newLink.mappable = connection;
			newLink.next = newNode;
			newLink.previous = connectedNode;

			//Add the link to the two nodes 
			connectedNode.paths.Add(newLink);
			newNode.paths.Add(newLink);

			//Add node and mappable to lists
			areaList.Add(newArea);
			areaList.Add(connection);

			ConnectAreas(newArea, connectedArea, connection);
		}
	}

	public void AddLink(IMappable area1, IMappable area2, IConnection connection)
	{
		Node node1 = null, node2 = null;
		foreach(Node node in nodeList)
		{
			//Assign the nodes in the nodeList to node1 and node2 if they exist. 
			if(node.mappable == area1) node1 = node;
			else if(node.mappable == area2) node2 = node;
		}

		//If the nodes do exist, create link and add it to nodes. 
		if(node1 != null && node2 != null)
		{
			//Create link, and populate it with the nodes and mappable. 
			Link newLink = new Link();
			newLink.mappable = connection;
			newLink.next = node2;
			newLink.previous = node1;

			//Assign the link to the nodes. 
			node1.paths.Add(newLink);
			node2.paths.Add(newLink);

			//Add mappable to lists
			areaList.Add(connection);
			ConnectAreas(area1, area2, connection);
		}
	}

	private IMappable GetClosestMappable(IMappable mappable)
	{
		float delta = 0;
		IMappable closest = null;

		foreach(IMappable area in areaList)
		{
			if(area != mappable)
			{
				float newDelta = (area.Position - mappable.Position).magnitude;
				
				if(newDelta < delta || delta == 0)
				{
					delta = newDelta;
					closest = area;
				}
			}
		}

		return closest;
	}

	private void ConnectAreas(IMappable area1, IMappable area2, IConnection connection)
	{
		Vector2 point1 = new Vector2(), point2 = new Vector2();

		if(ConnectionPointCalculator.CalculatePoints(area1, area2, out point1, out point2))
		{
			connection.AddConnection(point1);
			connection.AddConnection(point2);
		}
	}

	bool IMap.GenerateMap()
	{
		Rect mapBounds = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
		
		//Get the dimensions of the map created by the areas. 
		foreach(IMappable area in areaList)
		{
			if(mapBounds.xMax < area.Position.x + area.Size.x)
				mapBounds.xMax = area.Position.x + area.Size.x;
			if(mapBounds.yMax < area.Position.y + area.Size.y)
				mapBounds.yMax = area.Position.y + area.Size.y;
			if(mapBounds.xMin > area.Position.x)
				mapBounds.xMax = area.Position.x;
			if(mapBounds.yMin > area.Position.y)
				mapBounds.yMax = area.Position.y;
		}
		
		grid.Init(mapBounds);


		foreach(IMappable area in areaList)
		{
			area.Generate(this);
		}

		return true;
	}
	
	//A class for the nodes of the web. These will be the rooms, and have references to the paths, or the links, to other nodes, if any.
	private class Node
	{
		public IMappable mappable;
		public List <Link> paths;

		public Node()
		{
			mappable = null;
			paths = new List<Link>();
		}
	}
	//A class fot the paths, or links, to different nodes. Contain references to two nodes, the one it comes from, and the one it's going to.
	private class Link
	{
		public IConnection  mappable;
		public Node previous, next;
		
		public Link()
		{
			mappable = null;
			previous = null;
			next = null;
		}

		public Node GetOtherNode(Node currentNode)
		{
			if(ReferenceEquals(currentNode, previous))
				return next;
			else
				return previous;
		}
	}
}


