using UnityEngine;
using System.Collections;

public class ImageGeneration
{
	public static Texture2D ToTexture(bool[,] rawImage)
	{
		int width = rawImage.GetLength(0);
		int height = rawImage.GetLength(1);
		
		Texture2D image = new Texture2D(width, height);
		
		
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
		{
			Color colour = (rawImage[i,j]) ? Color.white : Color.black;
			
			image.SetPixel(i, j, colour);
		}
		return image;
	}
}

