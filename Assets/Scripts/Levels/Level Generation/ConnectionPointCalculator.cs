using UnityEngine;
using System.Collections;

public class ConnectionPointCalculator
{
	public static bool CalculatePoints(IMappable area1, IMappable area2, out Vector2 point1, out Vector2 point2)
	{
		point1 = new Vector2();
		point2 = new Vector2();

		if(area1 != area2)
		{

			//If the areas can be connected by a straight vertical path.
			if(area2.Position.x > area1.Position.x - area2.Size.x && area2.Position.x < area1.Position.x + area1.Size.x)
			{
				int x = GetVerticalMidpoint(area1, area2);


				if(area1.Position.y > area2.Position.y)
				{
					point1 = new Vector2(x, area1.Position.y);
					point2 = new Vector2(x, area2.Position.y + area2.Size.y);
				}
				else
				{
					point1 = new Vector2(x, area1.Position.y + area1.Size.y);
					point2 = new Vector2(x, area2.Position.y);
				}
			}
			//If the areas can be connected by a straight horizontal path.
			else if(area2.Position.y > area1.Position.y - area2.Size.y && area2.Position.y < area1.Position.y + area1.Size.y)
			{
				int y = GetHorizontalMidpoint(area1, area2);
				
				if(area1.Position.x > area2.Position.x)
				{
					point1 = new Vector2(area1.Position.x, y);
					point2 = new Vector2(area2.Position.x + area2.Size.x, y);
				}
				else
				{
					point1 = new Vector2(area1.Position.x + area1.Size.x, y);
					point2 = new Vector2(area2.Position.x, y);
				}
			}
			else
			{
				int side = Random.Range(0, 2);

				//If area2 is left of area1
				if(area2.Position.x + area2.Size.x < area1.Position.x)
				{
					//If area2 is above area1
					if(area2.Position.y + area2.Size.y < area1.Position.y)
					{
						//Use random number to choose a side to start from
						if(side == 0)
						{
							//Left of area1, bottom of area2
							point1 = area1.LeftMiddle();
							point2 = area2.MiddleBottom();
						}
						else
						{
							//Top of area1, right of area2
							point1 = area1.MiddleTop();
							point2 = area2.RightMiddle();
						}
					}
					//Else area2 is below area1
					else
					{
						//Use random number to choose a side to start from
						if(side == 0)
						{
							//Left of area1, top of area2
							point1 = area1.LeftMiddle();
							point2 = area2.MiddleTop ();
						}
						else
						{
							//Bottom of area1, right of area2
							point1 = area1.MiddleBottom();
							point2 = area2.RightMiddle ();
						}
					}
				}
				//Else area2 is right of area1
				else
				{
					//If area2 is above area1
					if(area2.Position.y + area2.Size.y < area1.Position.y)
					{
						//Use random number to choose a side to start from
						if(side == 0)
						{
							//Right of area1, bottom of area2
							point1 = area1.RightMiddle();
							point2 = area2.MiddleBottom();
						}
						else
						{
							//Top of area1, Left of area2
							point1 = area1.MiddleTop();
							point2 = area2.LeftMiddle();
						}
					}
					//Else area2 is below area1
					else
					{
						//Use random number to choose a side to start from
						if(side == 0)
						{
							//Right of area1, top of area2
							point1 = area1.RightMiddle();
							point2 = area2.MiddleTop();
						}
						else
						{
							//Bottom of area1, Left of area2
							point1 = area1.MiddleBottom();
							point2 = area2.LeftMiddle();
						}
					}
				}
			}
			return true;
		}

		return false;
	}

	private static int GetVerticalMidpoint(IMappable area1, IMappable area2)
	{
		int x1, x2;
		
		if(area1.Position.x > area2.Position.x)
			x1 = (int)area2.Position.x;
		else
			x1 = (int)area1.Position.x;
		if(area1.Position.x + area1.Size.x > area2.Position.x + area2.Size.x)
			x2 = (int)area1.Position.x + (int)area1.Size.x;
		else
			x2 = (int)area2.Position.x + (int)area2.Size.x;
		
		return (x1 + x2)/2;
	}

	private static int GetHorizontalMidpoint(IMappable area1, IMappable area2)
	{
		int y1, y2;
		
		if(area1.Position.y > area2.Position.y)
			y1 = (int)area2.Position.y;
		else
			y1 = (int)area1.Position.y;
		if(area1.Position.y + area1.Size.y > area2.Position.y + area2.Size.y)
			y2 = (int)area1.Position.y + (int)area1.Size.y;
		else
			y2 = (int)area2.Position.y + (int)area2.Size.y;
		
		return (y1 + y2)/2;
	}
}

