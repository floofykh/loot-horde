using UnityEngine;
using System.Collections;

public class PrototypeScript : MonoBehaviour
{
	private ILevelGenerator levelGenerator;
	private IMap mapType;
	public GameObject playerPrefab;
	private bool isLoading = true;
	public GameObject bulletPrototype;

	// Use this for initialization
	void Start ()
	{
		var resourceManager = gameObject.GetComponent<ResourceManager>();
		if(resourceManager == null)
			Debug.LogError("Resource Manager not found");

		resourceManager.AddResource("PlayerPrefab", playerPrefab);

		var pool = GetComponent<GameObjectPool>();
		if(pool == null)
			Debug.LogError("GameObjectPool not found");

		pool.AddToPool(bulletPrototype, 10);

		mapType = new UniformlyAlignedMap();
		levelGenerator = new PrototypeLevelGeneration();
		levelGenerator.Init(mapType);
		levelGenerator.GenerateLevel();
		isLoading = false;
	}
	
	void OnGUI()
	{
		if(isLoading)
		{
			GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "Loading...");
		}
	}
}

