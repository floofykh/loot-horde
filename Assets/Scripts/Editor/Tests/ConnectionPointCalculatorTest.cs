using UnityEngine;
using System.Collections;
using NUnit.Framework;

[TestFixture]
public class ConnectionPointCalculatorTest
{
	[Test]
	public void TestUniformAlignedHorizontalAndVerticalPaths()
	{
		Vector2 [] positions = new Vector2[5];
		positions[0] = new Vector2(9,9);
		positions[1] = new Vector2(1,9);
		positions[2] = new Vector2(9,4);
		positions[3] = new Vector2(18,9);
		positions[4] = new Vector2(9,18);

		IMappable [] areas = new IMappable[5];

		for(int i=0; i<5; i++)
		{
			areas[i] = new LargeArea(positions[i]);
		}

		Vector2 point1, point2;
		bool result;

		result = ConnectionPointCalculator.CalculatePoints(areas[0], areas[1], out point1, out point2);
		Assert.IsTrue(result);
		Assert.AreEqual(new Vector2(9, 10), point1);
		Assert.AreEqual(new Vector2(3, 10), point2);
		result = ConnectionPointCalculator.CalculatePoints(areas[0], areas[2], out point1, out point2);
		Assert.IsTrue(result);
		Assert.AreEqual(new Vector2(10, 9), point1);
		Assert.AreEqual(new Vector2(10, 6), point2);
		result = ConnectionPointCalculator.CalculatePoints(areas[0], areas[3], out point1, out point2);
		Assert.IsTrue(result);
		Assert.AreEqual(new Vector2(11, 10), point1);
		Assert.AreEqual(new Vector2(18, 10), point2);
		result = ConnectionPointCalculator.CalculatePoints(areas[0], areas[4], out point1, out point2);
		Assert.IsTrue(result);
		Assert.AreEqual(new Vector2(10, 11), point1);
		Assert.AreEqual(new Vector2(10, 18), point2);
	}

	//Following two tests only work when the randomly generated number used when making paths correspond with the function name. 
	/*[Test]
	public void TestUnalignedPaths_RandomelyGenerated1()
	{
		Vector2 [] positions = new Vector2[5];
		positions[0] = new Vector2(9,9);
		positions[1] = new Vector2(1,1);
		positions[2] = new Vector2(17,4);
		positions[3] = new Vector2(18,15);
		positions[4] = new Vector2(5,14);
		
		Vector2 size = new Vector2(2,2);
		
		IMappable [] areas = new IMappable[5];
		
		for(int i=0; i<5; i++)
		{
			areas[i] = new LargeArea(positions[i], size);
		}
		
		Vector2 point1, point2;
		bool result;
		
		result = ConnectionPointCalculator.CalculatePoints(areas[0], areas[1], out point1, out point2);
		Assert.IsTrue(result);
		Assert.AreEqual(areas[0].MiddleTop(), point1);
		Assert.AreEqual(areas[1].RightMiddle(), point2);
		result = ConnectionPointCalculator.CalculatePoints(areas[0], areas[2], out point1, out point2);
		Assert.IsTrue(result);
		Assert.AreEqual(areas[0].MiddleTop(), point1);
		Assert.AreEqual(areas[2].LeftMiddle(), point2);
		result = ConnectionPointCalculator.CalculatePoints(areas[0], areas[3], out point1, out point2);
		Assert.IsTrue(result);
		Assert.AreEqual(areas[0].MiddleBottom(), point1);
		Assert.AreEqual(areas[3].LeftMiddle(), point2);
		result = ConnectionPointCalculator.CalculatePoints(areas[0], areas[4], out point1, out point2);
		Assert.IsTrue(result);
		Assert.AreEqual(areas[0].MiddleBottom(), point1);
		Assert.AreEqual(areas[4].RightMiddle(), point2);
	}

	//Following two tests only work when the randomly generated number used when making paths correspond with the function name. 
	[Test]
	public void TestUnalignedPaths_RandomelyGenerated0()
	{
		Vector2 [] positions = new Vector2[5];
		positions[0] = new Vector2(9,9);
		positions[1] = new Vector2(1,1);
		positions[2] = new Vector2(17,4);
		positions[3] = new Vector2(18,15);
		positions[4] = new Vector2(5,14);
		
		Vector2 size = new Vector2(2,2);
		
		IMappable [] areas = new IMappable[5];
		
		for(int i=0; i<5; i++)
		{
			areas[i] = new LargeArea(positions[i], size);
		}
		
		Vector2 point1, point2;
		bool result;
		
		result = ConnectionPointCalculator.CalculatePoints(areas[0], areas[1], out point1, out point2);
		Assert.IsTrue(result);
		Assert.AreEqual(areas[0].LeftMiddle(), point1);
		Assert.AreEqual(areas[1].MiddleBottom(), point2);
		result = ConnectionPointCalculator.CalculatePoints(areas[0], areas[2], out point1, out point2);
		Assert.IsTrue(result);
		Assert.AreEqual(areas[0].RightMiddle(), point1);
		Assert.AreEqual(areas[2].MiddleBottom(), point2);
		result = ConnectionPointCalculator.CalculatePoints(areas[0], areas[3], out point1, out point2);
		Assert.IsTrue(result);
		Assert.AreEqual(areas[0].RightMiddle(), point1);
		Assert.AreEqual(areas[3].MiddleTop(), point2);
		result = ConnectionPointCalculator.CalculatePoints(areas[0], areas[4], out point1, out point2);
		Assert.IsTrue(result);
		Assert.AreEqual(areas[0].LeftMiddle(), point1);
		Assert.AreEqual(areas[4].MiddleTop(), point2);
	}*/
}

