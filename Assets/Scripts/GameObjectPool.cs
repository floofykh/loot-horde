using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameObjectPool : MonoBehaviour
{
	//A collection representing the pool. The first GameObject is the prefab of 
	//which the gameobjects originate from. The second is a list of all the 
	//instantiated game objects of that prefab. 
	private Dictionary <GameObject, Stack<GameObject>> pool;

	// Use this for initialization
	void Awake ()
	{
		pool = new Dictionary <GameObject, Stack<GameObject>>();
	}

	public GameObject GetInstanceOfPrefab(GameObject prefab)
	{
		//If the pool has instantiated clones of the prefab before. 
		if(pool.ContainsKey(prefab))
		{
			Stack<GameObject> objects = pool[prefab];
			//If the list of game objects has been initilised and isn't empty
			if(objects != null && objects.Count > 0)
			{
				GameObject topObject = objects.Pop();

				//If the top object is null, instantiate it using the prefab.
				if(topObject == null)
					topObject = GameObject.Instantiate(prefab) as GameObject;
				else //Else set it to active. 
					topObject.SetActive(true);

				return topObject;
			}
			else //Else create a new list of gameobjects, and return a new instantiated object. 
			{
				if(objects == null)
					objects = new Stack<GameObject>();
				return GameObject.Instantiate(prefab) as GameObject;
			}
		}
		else //Else create a new list of the prefab's gameobjects and create and return a new one. 
		{
			pool.Add(prefab, new Stack<GameObject>());
			return GameObject.Instantiate(prefab) as GameObject;
		}
	}

	public void AddToPool(GameObject gameObject, GameObject prefab)
	{
		gameObject.SetActive(false);

		if(pool.ContainsKey(prefab))
		{
			Stack <GameObject> objects = pool[prefab];

			if(objects != null)
				objects.Push(gameObject);
			else
			{
				objects = new Stack<GameObject>();
				objects.Push(gameObject);
			}
		}
		else
		{
			pool.Add(prefab, new Stack<GameObject>());
			pool[prefab].Push(gameObject);
		}
	}

	public void AddToPool(GameObject prefab, int number)
	{
		if(pool.ContainsKey(prefab))
		{
			Stack<GameObject> objects = pool[prefab];

			if(objects == null)
				objects = new Stack<GameObject>();

			for(int i=0; i<number; i++)
			{
				GameObject newObject = GameObject.Instantiate(prefab) as GameObject;
				newObject.SetActive(false);
				objects.Push(newObject);
			}
		}
		else
		{
			pool.Add(prefab, new Stack<GameObject>());
			for(int i=0; i<number; i++)
			{
				GameObject newObject = GameObject.Instantiate(prefab) as GameObject;
				newObject.SetActive(false);
				pool[prefab].Push(newObject);
			}
		}
	}
}

