using UnityEngine;
using System.Collections;
using System.Xml.Linq;

public class DataReader
{
	private XDocument document;
	private XElement currentElement;

	public DataReader()
	{
		document = null;
		currentElement = null;
	}

	public bool Load(string documentPath)
	{
		try
		{
			document = XDocument.Load(documentPath);
		}
		catch(System.Exception e)
		{
			Debug.LogException(e);
			return false;
		}

		return true;
	}

	public bool GoToChildElement(string elementName)
	{
		if(document == null)
		{
			Debug.LogError("Document not loaded");
			return false;
		}

		try
		{
			currentElement = document.Element(elementName);
		}
		catch(System.Exception e)
		{
			Debug.LogException(e);
			return false;
		}

		return true;
	}

	public string GetAttributeValue(string attribute)
	{
		if(currentElement != null)
		{
			return currentElement.Attribute(attribute).Value;
		}
		return null;
	}


}

