using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;

public class ResourceManager : MonoBehaviour
{
	public TextAsset ApplicationResourcesXML;
	private XDocument document;

	private Dictionary<string, Object> resources;

	// Use this for initialization
	void Awake ()
	{
		resources = new Dictionary<string, Object>();
		document = XDocument.Parse(ApplicationResourcesXML.text);
	}

	public string GetResource (string path, string attributeName)
	{
		string [] splitPath = path.Split('/');
		XElement currentElement = document.Element("ApplicationResources");

		foreach(string elementName in splitPath)
		{
			currentElement = currentElement.Element(elementName);
		}

		if(currentElement != null && currentElement.HasAttributes)
		{
			try
			{
				return currentElement.Attribute(attributeName).Value;
			}
			catch(System.NullReferenceException e)
			{
				Debug.LogError(e);
				return null;
			}
		}

		return null;
	}

	public Object GetResource(string name)
	{
		if(resources.ContainsKey(name))
			return resources[name];
		else
			return null;
	}

	public bool AddResource(string name, Object resource)
	{
		if(resources.ContainsKey(name))
			return false;
		else
		{
			resources.Add(name, resource);
			return true;
		}
	}

	public bool SetResource(string name, Object resource)
	{
		if(resources.ContainsKey(name))
		{
			resources[name] = resource;
			return true;
		}
		else
			return false;
	}

	public bool RemoveResource(string name)
	{
		return resources.Remove(name);
	}

	public Dictionary<string, string> GetResources(string path)
	{
		Dictionary<string, string> returnDictionary;

		string [] splitPath = path.Split('/');
		XElement currentElement = document.Element("ApplicationResources");
		
		foreach(string elementName in splitPath)
		{
			currentElement = currentElement.Element(elementName);
		}
		
		if(currentElement != null && currentElement.HasAttributes)
		{
			returnDictionary = new Dictionary<string, string>();

			foreach(XAttribute attribute in currentElement.Attributes())
			{
				returnDictionary.Add(attribute.Name.LocalName, attribute.Value);
			}

			return returnDictionary;
		}
		
		return null;

	}
}

