using UnityEngine;
using System.Collections;

public class GeometryAlgebra
{
	/** 
	 * Functions for equations of a straight line.
	 * 
	 * Get gradient.
	 **/
	public static float CalculateGradient(Vector2 point1, Vector2 point2)
	{
		//Gradient = Change in Y / Change in X
		
		return (point2.y - point1.y) / (point2.x - point1.x);
	}

	//Get the Y Intercept. Or the 'c' in y = mx + c
	public static float CalculateYIntercept(float gradient, Vector2 point)
	{
		//Equation of a line: y = mx + c;
		//Therefore c = y - mx;
		
		return point.y - gradient*point.x;
	}
	
	public static float CalculateYIntercept(Vector2 point1, Vector2 point2)
	{
		return point1.y - CalculateGradient(point1, point2)*point1.x;
	}

	//Calculate whether the line created by two points intercept with a straight vertical/horizontal line. 
	public static bool HasIntercept(Vector2 point1, Vector2 point2, float value, bool xAxis)
	{
		float lesserValue, greaterValue;

		if(xAxis)
		{
			if(point1.x < point2.x)
			{
				lesserValue = point1.x;
				greaterValue = point2.x;
			}
			else if(point1.x > point2.x)
			{
				lesserValue = point2.x;
				greaterValue = point1.x;
			}
			else
			{
				//Points are the same;
				return false;
			}
			if(value > lesserValue && value < greaterValue)
				return true;
		}
		else
		{
			if(point1.y < point2.y)
			{
				lesserValue = point1.y;
				greaterValue = point2.y;
			}
			else if(point1.y > point2.y)
			{
				lesserValue = point2.y;
				greaterValue = point1.y;
			}
			else
			{
				//Points are the same;
				return false;
			}
			if(value > point1.y && value < point2.y)
				return true;
		}

		return false;
	}

	public static Vector2 CalculateIntercept(float gradient1, float yIntercept1, float gradient2, float yIntercept2)
	{
		/*Equation of a line: y = mx + c
		 *m1*x + c1 = m2*x + c2
		 *m1*x - m2*x = c2 - c1
		 *x = (c2 - c1) / (m1 - m2)
			*/

		//Ensure lines are not parallel
		if(gradient1 != gradient2)
		{
			Vector2 point = new Vector2();
			point.x = (yIntercept2 - yIntercept1) / (gradient1 - gradient2);

			//Substitute x-coordinate into equation along with gradient and yIntercept to get y-coordinate.
			point.y = gradient1*point.x + yIntercept1;

			return point;
		}

		return new Vector2();
	}
	
	public static bool PointLiesInCircle(Vector2 point, Vector2 circleCentre, float circleRadius)
	{
		return ((point.x - circleCentre.x)*(point.x - circleCentre.x) + (point.y - circleCentre.y)*(point.y - circleCentre.y) <= circleRadius*circleRadius);
	}
}

